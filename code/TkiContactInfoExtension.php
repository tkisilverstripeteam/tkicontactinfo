<?php


class TkiContactInfoExtension extends DataExtension {

	private static $db = array(
		'LocationName'  => 'Varchar(255)',
		'Address'  => 'Varchar(255)',
		'City'   => 'varchar(64)',
		'State'    => 'Varchar(64)',
		'Postcode' => 'Varchar(10)',
		'Country'  => 'Varchar(2)',
		'UseMailingAddress' => 'Boolean',
		'MailingName'  => 'Varchar(255)',
		'MailingAddress'  => 'Varchar(255)',
		'MailingCity'   => 'varchar(64)',
		'MailingState'    => 'Varchar(64)',
		'MailingPostcode' => 'Varchar(10)',
		'MailingCountry'  => 'Varchar(2)',
		'Phone' => 'Varchar(32)',
		'Mobile' => 'Varchar(32)',
		'TollFree' => 'Varchar(32)',
		'Fax' => 'Varchar(32)',
		'Email' => 'Varchar(100)'
	);

	private static $casting = array(
        'LocationNameOrTitle' => 'Varchar',
		'CountryName' => 'Varchar',
		'StateName' => 'Varchar',
        'MailingNameOrTitle' => 'Varchar',
		'MailingCountryName' => 'Varchar',
		'MailingStateName' => 'Varchar'
	);

    /**
     * Disabled sections: AddressSection, MailingAddressSection,ContactSection
     * @config
     * @array type 
     */
    private static $contactinfo_disabled_sections;
    
    
	/**
	 * Array of ISO country codes
	 * @config
	 * @var array
	 */
	private static $allowed_countries;

	/**
	 * Array of ISO subdivision codes
	 * @config
	 * @var DataList
	 */
	private static $allowed_subdivisions;

	/**
	 * Array of ISO subdivision codes
	 * @config
	 * @var DataList
	 */
	private static $excluded_subdivision_types;

	/**
	 * Instance value
	 * @var DataList
	 */
	protected $allowedCountries;

	/**
	 * Instance cache
	 * @var array
	 */
	protected $countryOptions;

	/**
	 * Instance value
	 * @var array
	 */
	protected $allowedSubdivisions;

	/**
	 * Instance cache
	 * @var array
	 */
	protected $subdivisionOptions;

	/**
	 * Instance cache
	 * @var string
	 */
	protected $countryName;

	/**
	 * Instance cache
	 * @var string
	 */
	protected $stateName;

	/**
	 * Instance cache
	 * @var string
	 */
	protected $mailingCountryName;

	/**
	 * Instance cache
	 * @var string
	 */
	protected $mailingStateName;

	/*
	 * -------------------------------------------------------------------------
	 * Getters / setters
	 * -------------------------------------------------------------------------
	 */

	/**
	 *
	 * @return DataList
	 */
	public function getAllowedCountries()
	{
		if(!is_null($this->allowedCountries)) {
			return $this->allowedCountries;
		}

		$codes = Config::inst()->get(get_class($this->owner),'allowed_countries');

		// Filter by configured codes
		if(is_array($codes) && !empty($codes)) {
			$this->allowedCountries = Country::get()->filter(array(
				'ISO1A2' => $codes
			));
		}
		// Not configured, get all
		else {
			$this->allowedCountries = Country::get()
                ->where("\"ISO1A2\" IS NOT NULL");
        }

        return $this->allowedCountries;
    }

    public function setAllowedCountries(DataList $list)
    {
        $this->allowedCountries = $list;
    }

    /**
     *
     * @return DataList
     */
    public function getAllowedSubdivisions()
    {
        if (!empty($this->allowedSubdivisions)) {
            return $this->allowedSubdivisions;
        }

        $codes = Config::inst()->get(get_class($this->owner), 'allowed_subdivisions');

        // Filter by configured codes
        if (is_array($codes) && !empty($codes)) {
            $this->allowedSubdivisions = CountrySubdivison::get()->filter(array(
                'ISO2' => $codes
            ));
        }
        // Not configured, filter by allowed countries
        else {
            $countryIDs = $this->getAllowedCountries()->column('ID');
            $this->allowedSubdivisions = CountrySubdivison::get()->filter(array(
                'CountryID' => $countryIDs
            ));
        }

        return $this->allowedSubdivisions;
    }

    public function setAllowedSubdivisions(DataList $list)
    {
        $this->allowedSubdivisions = $list;
    }
    /*
     * -------------------------------------------------------------------------
     * Admin
     * -------------------------------------------------------------------------
     */


	public function updateCMSFields(FieldList $fields) 
    {

		Requirements::css('tkicontactinfo/css/admin.css');

        // Check for "Main" tab
        $mainTab = $fields->fieldByName('Root.Main');
        $hasMainTab = ($fields->hasTabSet() && $mainTab && $mainTab instanceof Tab);
        
        $disabledSections = (array) $this->owner->config()->get('contactinfo_disabled_sections',Config::UNINHERITED);
        
        /*
         * Address section
         */
        if(!in_array('AddressSection',$disabledSections)) {
            $addressSection = $this->createAddressFields(!$hasMainTab);
            if($hasMainTab)  { 
                // Address tab
                $addressTab = Tab::create('Address',_t('TkiContactInfoExtension.AddressTab','Address'));
                $fields->insertAfter('Main',$addressTab);
                $fields->addFieldToTab('Root.Address',$addressSection);
            } else {
                // No tabs
                $fields->push($addressSection);
            }
        }
        /*
         * Mailing address section
         */
        if(!in_array('MailingAddressSection',$disabledSections)) {
            $mailsection = $this->createMailingAddressFields(isset($addressSection));
            
            if($hasMainTab)  { 
                // Address tab
                if(!isset($addressTab)) {
                    $addressTab = Tab::create('Address',_t('TkiContactInfoExtension.AddressTab','Address'));
                    $fields->insertAfter('Main',$addressTab);
                }
                $fields->addFieldToTab('Root.Address',$mailsection);
                // Display logic
                if(isset($addressSection)) {
                    $mailsection->hideUnless('UseMailingAddress')->isChecked();
                }
            } else {
                // No tabs
                $fields->push($mailsection);    
            }
        }
        
        /*
         * Contact section
         */
		$contactSection = $this->createContactFields(!$hasMainTab);
        if(!in_array('ContactSection',$disabledSections)) {
            // Contact tab
            if($hasMainTab)  {
                $contactTab = Tab::create('Contact',_t('TkiContactInfoExtension.ContactTab','Contact'));
                $fields->insertAfter('Main',$contactTab);
                $fields->addFieldToTab('Root.Contact',$contactSection);
            } else {
                $fields->push($contactSection);
            }
        }
		
	}

	/**
	 * @return array
	 */
	public function createAddressFields($includeHeader=true) {

		$fields = array(
			TextField::create('LocationName', _t('TkiContactInfoExtension.LocationName', 'Name'))->setDescription(_t('TkiContactInfoExtension.LocationNameDescription', 'Optional')),
			TextField::create('Address', _t('TkiContactInfoExtension.Address', 'Address')),
			TextField::create('City', _t('TkiContactInfoExtension.City', 'City')),
			DropdownField::create('Country', _t('TkiContactInfoExtension.Country', 'Country'),$this->getCountryOptions())
                ->setHasEmptyDefault(true)
                ->setEmptyString(_t('TkiContactInfoExtension.CountryEmptyString', 'Select country')),
            GroupedDropdownField::create('State', _t('TkiContactInfoExtension.State', 'State'), $this->getSubdivisionOptions())
                ->setHasEmptyDefault(true),
			TextField::create('Postcode', _t('TkiContactInfoExtension.Postcode', 'Postcode'))
		);
        $disabledSections = (array) $this->owner->config()->get('contactinfo_disabled_sections',Config::UNINHERITED);
        if(!in_array('MailingAddressSection',$disabledSections)) {
            $fields[] = CheckboxField::create('UseMailingAddress',_t('TkiContactInfoExtension.UseMailingAddress', 'Use different address for mail'));
        }
			
		if($includeHeader) {
			array_unshift($fields,HeaderField::create('AddressHeader',_t('TkiContactInfoExtension.AddressHeader', 'Location')));
		}

        // Address header
        if ($includeHeader) {
            array_unshift($fields, HeaderField::create('AddressHeader', _t('TkiContactInfoExtension.AddressHeader', 'Location')));
        }

        return DisplayLogicWrapper::create($fields);
    }

    /**
     * @return array
     */
    public function createMailingAddressFields($includeHeader = true)
    {

		$fields = array(
			TextField::create('MailingName', _t('TkiContactInfoExtension.MailingName', 'Name'))->setDescription(_t('TkiContactInfoExtension.MailingNameDescription', 'Optional')),
			TextField::create('MailingAddress', _t('TkiContactInfoExtension.MailingAddress', 'MailingAddress')),
			TextField::create('MailingCity', _t('TkiContactInfoExtension.MailingCity', 'City')),
			DropdownField::create('MailingCountry', _t('TkiContactInfoExtension.MailingCountry', 'Country'),$this->getCountryOptions())
                ->setHasEmptyDefault(true)
                ->setEmptyString(_t('TkiContactInfoExtension.CountryEmptyString', 'Select country')),
            GroupedDropdownField::create('MailingState', _t('TkiContactInfoExtension.MailingState', 'State'), $this->getSubdivisionOptions())
                ->setHasEmptyDefault(true)
                ->setEmptyString(_t('TkiContactInfoExtension.SubdivisionEmptyString','Select province / state')),
			TextField::create('MailingPostcode', _t('TkiContactInfoExtension.MailingPostcode', 'Postcode'))
		);

		if($includeHeader) {
			array_unshift($fields,HeaderField::create('MailingAddressHeader',_t('TkiContactInfoExtension.MailingAddressHeader', 'Mail')));
		}

		return DisplayLogicWrapper::create($fields);
	}

	/**
	 * @return array
	 */
	public function createContactFields($includeHeader=true) {
		$fields = array(
			EmailField::create('Email', _t('TkiContactInfoExtension.Email', 'Email')),
			TextField::create('Phone', _t('TkiContactInfoExtension.Phone', 'Phone')),
			TextField::create('Mobile', _t('TkiContactInfoExtension.Mobile', 'Mobile')),
			TextField::create('TollFree', _t('TkiContactInfoExtension.TollFree', 'Toll free phone')),
			TextField::create('Fax', _t('TkiContactInfoExtension.Fax', 'Fax')),
		);
		if($includeHeader) {
			array_unshift($fields,HeaderField::create('ContactHeader',_t('TkiContactInfoExtension.ContactHeader', 'Contact')));
		}
		return DisplayLogicWrapper::create($fields);
	}


	/*
	 * -------------------------------------------------------------------------
	 * Templates
	 * -------------------------------------------------------------------------
	 */
    
    /**
     * Gets MailingName, falling back to owner Title or Name field, if available.
     * @return string
     */
    public function getLocationNameOrTitle()
	{
        if(!empty($this->owner->LocationName)) {
            return $this->owner->LocationName;
        } elseif(!empty($this->owner->Title)) {
            return $this->owner->Title;
        } elseif(!empty($this->owner->Name)) {
            return $this->owner->Name;
        }
		return '';
    }
    
	public function getLocationAddressHTML()
	{
		return $this->owner->renderWith('LocationAddress');
	}

	public function getContactDetailsHTML()
	{
		return $this->owner->renderWith('ContactDetails');
	}

	public function LocationMapHTML($width,$height)
    {
		$data = ArrayData::create(array(
			'Width'    => $width,
			'Height'   => $height,
			'Address' => rawurlencode($this->getLocationAddressString()),
			'ApiKey' => SiteConfig::current_site_config()->MapsApiKey
		));
		return $data->renderWith('LocationMap');
	}

	public function getLocationAddressString()
	{
		$arr = array(
			$this->owner->Address,
			$this->owner->City,
			$this->owner->getStateName(),
			$this->owner->getCountryName()
		);
		$str = implode(', ',array_filter($arr));
		return trim(preg_replace('/\n+/',', ', $str));
	}

	/**
	 * Determines whether object has minimum location fields required for mapping
	 * @return boolean
	 */
	public function HasContactDetails()
	{
		return ($this->owner->Phone
			|| $this->owner->Mobile
			|| $this->owner->TollFree
			|| $this->owner->Fax
			|| $this->owner->Email
		);
	}

	/**
	 * Determines whether object has minimum location fields
	 * @return boolean
	 */
	public function HasLocationAddress()
	{
		return ($this->owner->Address
			&& $this->owner->City
			&& $this->owner->Country
		);
	}

	public function IsMappable()
	{
		return $this->HasLocationAddress() && SiteConfig::current_site_config()->MapsApiKey;
	}

	/**
	 * Determines whether object has mailing information
	 * @return boolean
	 */
	public function HasMailingAddress()
	{
		return ($this->owner->UseMailingAddress
			&& $this->owner->Address
			&& $this->owner->City
			&& $this->owner->Country
		);
	}

    /**
     * Gets MailingName, falling back to owner Title or Name field, if available.
     * @return string
     */
    public function getMailingNameOrTitle()
	{
        if(!empty($this->owner->MailingName)) {
            return $this->owner->MailingName;
        } elseif(!empty($this->owner->Title)) {
            return $this->owner->Title;
        } elseif(!empty($this->owner->Name)) {
            return $this->owner->Name;
        }
		return '';
    }
    
	public function getMailingAddressHTML()
	{
		return $this->owner->renderWith('MailingAddress');
	}

   
	public function getStateName()
	{
		if(is_null($this->stateName)) {
			$this->stateName = $this->findStateName($this->owner->State);
		}
		return $this->stateName;
	}

	public function getCountryName()
	{
		if(is_null($this->countryName)) {
			$this->countryName = $this->findCountryName($this->owner->Country);
		}
		return $this->countryName;
	}

	public function getMailingStateName()
	{
		if(is_null($this->mailingStateName)) {
			$this->mailingStateName = $this->findStateName($this->owner->MailingState);
		}
		return $this->mailingStateName;
	}

	public function getMailingCountryName()
	{
		if(is_null($this->mailingCountryName)) {
			$this->mailingCountryName = $this->findCountryName($this->owner->MailingCountry);
		}
		return $this->mailingCountryName;
	}
    
    public function getLocationName()
	{
		return trim($this->owner->getField('LocationName'));
	}

	public function getMailingName()
	{
		return trim($this->owner->getField('MailingName'));
	}
    
    protected function removeDisabledFields(\FieldList &$fields)
    {
        $disabled = $this->getDisabledFields();
        
        if(!empty($disabled) && is_array($disabled)) {
            $fields->removeByName($disabled);
        }
    }
    
    /*
     * -------------------------------------------------------------------------
     * Helper methods
     * -------------------------------------------------------------------------
     */

    public function getCountryOptions()
    {
        if (!is_null($this->countryOptions)) {
            return $this->countryOptions;
        }
        $this->countryOptions = $this->getAllowedCountries()->map('ISO1A2', 'Name')->toArray();

        return $this->countryOptions;
    }

    /**
     *
     * @param string|array $allowedTypes
     * @return array
     */
    public function getSubdivisionOptions($excludedTypes = null)
    {
        if (!is_null($this->subdivisionOptions)) {
            return $this->subdivisionOptions;
        }

        $list = ArrayList::create($this->getAllowedSubdivisions()->toArray());

        // Exclude type(s)
        $excluded = ($excludedTypes) ? (array) $excludedTypes : Config::inst()->get(get_class($this->owner), 'excluded_subdivision_types');
        if (!empty($excluded)) {
            $list = $list->exclude('Type', $excluded);
        }

        $countries = $this->getAllowedCountries();

        // Create grouped list
        // Add empty string here, as GroupedDropdownField::setEmptyString() doesn't seem to work
        $this->subdivisionOptions = array(
            '' => _t('TkiContactInfoExtension.SubdivisionEmptyString', 'Select province / state')
        );
        
		foreach($countries as $country) {
			$this->subdivisionOptions[$country->Name] = $list->filter(array('CountryID' => $country->ID))
                ->sort('Name','ASC')
                ->map('ISO2','Name');
		}

		return $this->subdivisionOptions;
	}

	/**
	 * Find parent title/name
	 * @return string
	 
	protected function contactTitle()
	{
		if(!empty($this->owner->getField('Title'))) {
			return $this->owner->getField('Title');
		} elseif(!empty($this->owner->Name)) {
			return $this->owner->Name;
		} else {
			return '';
		}
	}
    */
	protected function findStateName($code)
	{
		if(empty($code)) {
			return '';
		}
		$obj = CountrySubdivison::get()->filter(array('ISO2' => $code))->first();
		return ($obj) ? $obj->Name : '';
	}

	protected function findCountryName($code)
	{
		if(empty($code)) {
			return '';
		}
		$obj = Country::get()->filter(array('ISO1A2' => $code))->first();
		return ($obj) ? $obj->Name : '';
	}


    /**
     * Find parent title/name
     * @return string
     */
    protected function contactTitle()
    {
        if (!empty($this->owner->Title)) {
            return $this->owner->Title;
        } elseif (!empty($this->owner->Name)) {
            return $this->owner->Name;
        } else {
            return '';
        }
    }

}
