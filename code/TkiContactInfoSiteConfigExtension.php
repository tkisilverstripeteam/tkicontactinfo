<?php

class TkiContactInfoSiteConfigExtension extends DataExtension {

	private static $db = [
		'MapsApiKey' => 'Varchar(255)'
	];

	public function updateCMSFields(FieldList $fields)
    {

		/* ---- Maps ---- */
		$fields->findOrMakeTab(
			'Root.Maps', _t('TkiContactInfoSiteConfigExtension.MapsTab','Maps')
		);
	
		$fields->addFieldToTab('Root.Maps',
			TextField::create('MapsApiKey', _t('TkiContactInfoSiteConfigExtension.MapsApiKey', 'Google Maps API key'))
		);


    }

	public function setCreditURL($value)
	{
		$this->owner->setField('CreditURL',$this->prepareURL($value));
	}
	
	public function setFacebookURL($value)
	{
		$this->owner->setField('FacebookURL',$this->prepareURL($value));
	}
	
	public function setYouTubeURL($value)
	{
		$this->owner->setField('YouTubeURL',$this->prepareURL($value));
	}

	/**
	 * Adds scheme if missing
	 * @param  string $value
	 * @return string
	 */
	protected function prepareURL($value)
	{
		$scheme = parse_url($value,PHP_URL_SCHEME);
		if(!empty($value) && (empty($scheme) || strpos($value,'://' === false))) {
			$value = 'http://'. $value;
		}
		return $value;
	}
	
	public function initDefaultUploadsFolder()
	{
		if(!empty($this->owner->UploadsFolderID)) {
			$uploadsFolder = $this->owner->UploadsFolder()->Filename;
			// Remove assets folder name from upload folder path
			if(stripos($uploadsFolder, ASSETS_DIR) === 0) {
				$uploadsFolder = substr($uploadsFolder, strlen(ASSETS_DIR) + 1);
			}
			Config::inst()->update('Upload', 'uploads_folder', $uploadsFolder);
		}
	}
	
}
